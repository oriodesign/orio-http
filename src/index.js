class HttpClient {

    constructor(config) {
        this.config = Object.assign({}, {
            headers: new Headers()
        }, config);
    }

    setHeader = (key, value) => {
        this.config.headers.set(key, value);
    }

    request = (url, config) => {
        return fetch(url, config)
            .then(this.handleResponse)
            .then(this.parseJson)
            .catch(function (ex) {
                console.log('parsing failed', ex)
            });
    };

    get = (url) => {
        return request(url, Object.assign({}, this.config, {
            method: 'GET'
        }));
    };

    post = (url, body) => {
        return request(url, Object.assign({}, this.config, {
            method: 'POST',
            body: body
        }));
    };

    put = (url, body) => {
        return request(url, Object.assign({}, this.config, {
            method: 'PUT',
            body: body
        }));
    };

    delete = (url) => {
        return request(url, Object.assign({}, this.config, {
            method: 'DELETE'
        }));
    };

    handleResponse = (response) => {
        if (response.status >= 200 && response.status < 300) {
            return response.json();
        }

        var error = new Error(response.statusText)
        error.response = response
        throw error;
    };

    parseJson = (response) => {
        return response.json();
    }

}

export default HttpClient;